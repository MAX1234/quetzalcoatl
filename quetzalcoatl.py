# Quetzalcoatl.py
# You are free to distribute this file as long as you do not sell it,
# and you keep this notice (this comment, and the author notice).
# Violation of this notice will result in some bad thing happening to you.
# Author: NoOneIsHere (StackExchange)/MAX1234 (BitBucket)

from sys import argv, version_info, stderr, path
from re import *
from tokenize import *
import io
import os
import traceback
import bz2
path.insert(0, os.path.dirname(__file__))  # Add dir to path
exec('from q' + str(version_info[0]) + 'q import *')
for _i in ["time", "random", "base64", "math", "datetime"]:
    exec('from ' + _i + ' import *')


class QBlock:
    def __init__(self, da):
        self.data = da

    def __str__(self):
        return '{' + self.data + '}'

    def run(self):
        # printf(self.data)
        convert_to_python(self.data)

    def append(self, _dat):
        self.data += _dat


def super_tokenize(da):
    return [(i.string, tok_name[i.type], i.start, i.end) for i in list(tokenize(io.BytesIO(da.encode('utf-8')).readline))][1:]


def convert_to_python(string):

    _temp = super_tokenize(string)  # TODO: FIND WAY TO AUTO-INSERT SPACES INTO STRING
    delagator = {'+': 'stack.append(stack.pop() + stack.pop())', '-': 'stack.append(-stack.pop() + stack.pop())',
                 '*': 'stack.append(stack.pop() * stack.pop())', '/': 'stack.append(1 / stack.pop() * stack.pop())',
                 ',': 'stack.append([stack.pop() for i in range(stack.pop())])', '.': 'stack.extend([stack.pop()] * 2)',
                 '@': 'rotate()', '!': 'exc()',
                 '//': 'stack.append(int(1 / stack.pop() * stack.pop()))',
                 '|': 'stack.append(stack.pop() or stack.pop())', '&': 'stack.append(stack.pop() and stack.pop())',
                 '\\': 'stack.extend([stack.pop(), stack.pop()][::-1])',
                 '{': '_set=True', '}': 'stack.append(s);s=QBlock(\'\');_set=False',
                 '[': '_list=True', ']': 'stack.append(l);l=[];_list=False', '~': 'tilde()'}
    namedel = {'print': 'printf(stack.pop())', 'p': 'printf(stack.pop())', 'chomp': 'stack.append(c())',
               'c': 'stack.append(c(\'\'))', 'int': 'stack.append(int(stack.pop()))',
               'i': 'stack.append(int(stack.pop()))',
               'float': 'stack.append(float(stack.pop()))', 'f': 'stack.append(float(stack.pop()))',
               'string': 'stack.append(str(stack.pop()))', 's': 'stack.append(str(stack.pop()))',
               'while': 'while stack.pop(): string[i+1]', 'F': '_for()',
               'O': 'stack.append(ord(stack.pop()))',
               'C': 'stack.append(chr(stack.pop()))', 'r': 'stack.append(regexp())',
               'n': 'stack.append(not stack.pop())', 't': 'stack.append(time.time())',
               'R': 'stack.append(random())', 'E': 'stack.append(b64encode(stack.pop().encode(\'utf-8\')))',
               'd': 'stack.append(b64decode(stack.pop()))', 'ms': 'stack.append(sin(stack.pop()))',
               'mc': 'stack.append(cos(stack.pop()))', 'mt': 'stack.append(tan(stack.pop()))',
               'I': 'stack.append(1 / float(stack.pop()))', 'l': 'syscall()', 'e': 'echosyscall()',
               'S': 'stack.append(set(stack.pop()))', 'T': 'stack.append(tuple(stack.pop()))',
               'L': 'stack.append(list(stack.pop()))', 'N': 'stack.append(stack.pop() * -1)',
               'W': 'stack.extend(c(\' \'))', 'D': 'stack.append(datetime.now())', 'B': 'decompress()', 'b': 'comp()'}
    etdel = {'$': 'stack.remove(stack[-1])', ' ': ' ',
             '?': 'q()',
             '\\': 'stack.extend([stack.pop(), stack.pop()])'}
    s = QBlock('')
    _set = False
    l = []
    _list = False
    for i in range(len(_temp)):
        try:
            j = _temp[i]
            if _set and j[0] != '}':
                s.append(j[0])
                continue
            if j[1] == 'ERRORTOKEN':
                exec(etdel[j[0]])
            if j[1] == 'NUMBER':
                if _set:
                    s.add(int(j[0]) if i == 0 or _temp[i - 1][0] != '-' else -int(j[0]))
                elif _list:
                    l.append(int(j[0]) if i == 0 or _temp[i - 1][0] != '-' else -int(j[0]))
                else:
                    stack.append(int(j[0]) if i == 0 or _temp[i - 1][0] != '-' else -int(j[0]))
            if j[1] == 'STRING':
                if _set:
                    s.add(j[0][1:-1])
                elif _list:
                    l.append(j[0][1:-1])
                else:
                    stack.append(j[0][1:-1])
            if j[1] == 'OP':
                exec(delagator[j[0]])
                if j[0] == '[':
                    _list = True
                elif j[0] == '{':
                    _set = True
                elif j[0] == ']':
                    l = []
                    _list = False
                elif j[0] == '}':
                    s = set([])
                    _set = False
            if j[1] == 'NAME':
                try:
                    exec(namedel[j[0]])
                except KeyError:
                    _name = j[0]
                    _s = list(_name)
                    for k in _s:
                        if k in namedel.keys():
                            exec(namedel[k])
                        else:
                            raise KeyError
        except BaseException as e:
            stderr.write("\nTraceback (most recent call last):\n\tat character '%s' at (%s, %s) to (%s, %s) of '%s'\n"
                         % (j[0], j[2][0], j[2][1], j[3][0], j[3][1], ' '.join(string.split('\n'))[:-1]))
            stderr.write(traceback.format_exception_only(e.__class__, e)[0])
            exit(1)


def comp():
    stack.append(bz2.compress(stack.pop(), compresslevel=stack.pop()))


def decompress():
    stack.append(bz2.decompress(stack.pop()))


def tilde():
    x = stack.pop()
    if type(x) in [type([]), type(()), type(set())]:
        stack.extend(x)
    if type(x) == type(QBlock('')):
        x.run()


def q():
    x = stack.pop()
    y = stack.pop()
    stack.append(float(y) ** float(x))


def _for():
    _times = stack.pop()
    _delattr = stack.pop()
    for i in range(_times - 1):
        _delattr.run()


def _sys_get_argv(arg):
    x = []
    for i in arg[1:]:
        if i in ['-q', '-h', '-n', '-v']:
            x = [i]
        if i in ['-b', '-D']:
            x += [i]
        else:
            x = [i] + x
    return [__file__] + x

argv = _sys_get_argv(argv)


def syscall():
    os.system(stack.pop())


def echosyscall():
    os.system('echo -e ' + stack.pop())


def regexp():
    return match(stack.pop(), stack.pop())


def exc():
    _x = stack.pop()
    _y = stack.pop(-_x)
    stack.append(_y)


def c(devnull):
    if devnull:
        return chomp().split(devnull)
    else:
        return chomp()


# def convert_to_python(string):
#                 return string.replace('@', 'rotate()').replace('\\rotate()', '@')
# .replace('^^', 'hoist(').replace('\\hoist(', '^^').replace('c>', 'debase(')
# .replace('\\debase(', 'c>').replace('<c', 'base(').replace('\\base(', '<c')
# .replace('=>', '= lambda ').replace('\\=lambda ', '=>').replace('%%%', 'delimiter = ')
# .replace('\\delimiter = ', '%%%').replace('$', 'stack.remove(stack[len(stack) - 1])')
# .replace('->', 'stack.pop()').replace('\\stack.pop()', '->')
# .replace('\\stack.remove(stack[len(stack) - 1])', '$').replace('<-', 'stack = stack + ')
# .replace('\\stack = stack + ', '<-').replace('::', 'print ').replace('[print', '[::').replace('\\print', '\\::')
# string.replace(';', '\n').replace('\\\n', ';')


def rotate():
    if len(stack) < 3:
        if len(stack) == 2:
            stack[0], stack[1] = stack[1], stack[0]
    else:
        stack[-3], stack[-2], stack[-1] = stack[-2], stack[-1], stack[-3]


stack = []

delimiter = ' '

data = ""

_help = """
Use: python quetzalcoatl.py [-hnvq] [-b] [-D] [file.qz]
\t-h prints this help
\t-v prints the version
\t-n prints the name of this language (Qutezalcoatl)
\t-b suppresses warnings
\t-q outputs the source code instead of running the program.
\t-D prints out the tokenized data from the code.
YOU CAN ONLY USE ONE OF [nhvq] AT A TIME"""

_version = "2.0.0a0"

if __name__ == "__main__":
    if not argv[1:]:
        printf(_help)
        exit(0)

    if not ('-b' in argv or '--bash-warnings' in argv) and version_info >= (3, 0, 0):
        pass

    if '-n' in argv or '--name' in argv:
        printf('Quetzalcoatl')
        exit(0)

    if '-h' in argv or '--help' in argv:
        printf(_help)
        exit(0)

    if '-v' in argv or '--version' in argv:
        printf("Quetzalcoatl version: " + _version)
        printf("Python version: " + str(version_info[0]) + "." + str(version_info[1]) + "." + str(version_info[2]))
        exit(0)

    if '-c' in argv or '--compile-string' in argv:
        data = argv[2]
    else:
        with open(argv[1]) as dat:
            data = ''.join(dat.readlines())

    if '-D' in argv:
        printf(super_tokenize(data))

    if '-q' in argv or '--quine' in argv:
        print(data)
    else:
        convert_to_python(data)

    f(stack, delimiter)

    exit(0)
