###  Input

In Quetzalcoatl, the only way to get input is with `c`. For example, `c 3*` takes input, pushes `3`, and multiplies. If the input was `hi` the output would be `hihihi`.