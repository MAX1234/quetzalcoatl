from unittest import TestCase
import sys
import os
import math
sys.path.append(os.path.dirname(__file__))
import quetzalcoatl


class TestConvertToPython(TestCase):
    def test_convertToPython(self):
        arr1 = ['1 2 3 ++', '2 .I?', '1 2?', '1 2 \\']
        arr2 = [[6], [math.sqrt(2)], [1], [2, 1]]
        for n in range(len(arr1)):
            i = arr1[n]
            j = arr2[n]
            quetzalcoatl.convert_to_python(i)
            if not quetzalcoatl.stack == j:
                self.fail("Stack did not complete to expected value (%s, %s)" % (repr(quetzalcoatl.stack), repr(j)))
            else:
                quetzalcoatl.stack = []
