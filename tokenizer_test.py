import tokenize, io
src = input() or "1 2 3 ++ print"


def show_token(type_, token):
    print("%s\t%s" % \
          (tokenize.tok_name[type_], repr(token)))

for i in list(tokenize.tokenize(io.BytesIO(src.encode('utf-8')).readline)):
    print(show_token(i.type, i.string))
